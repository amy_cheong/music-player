//
//  SongList.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import Foundation

struct SongList: EntityType {
   var results: [Song]
}
