//
//  Song.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import Foundation

typealias EntityType = Codable & Equatable

struct Song: EntityType {
   var trackName: String?
   var artistName: String?
   var collectionName: String?
   var artworkUrl100: String?
   var previewUrl: String?
}
