//
//  APIRouter.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 7/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//
import Foundation

public enum RequestType: String {
   case GET     = "GET"
   case POST    = "POST"
}

protocol APIRequest {
   typealias Parameters = [String: String]

   var method: RequestType { get }
   var path: String { get }
   var parameters: Parameters { get }
}

extension APIRequest {
   func request(with baseURL: URL) -> URLRequest {
      guard var components = URLComponents(url: baseURL.appendingPathComponent(path),
                                           resolvingAgainstBaseURL: false) else {
         fatalError("Unable to create URL components")
      }
      
      components.queryItems = parameters.map {
         URLQueryItem(name: String($0), value: String($1))
      }
      
      guard let url = components.url else {
         fatalError("Could not get url")
      }
      
      var request = URLRequest(url: url)
      request.httpMethod = method.rawValue
      request.addValue("application/json", forHTTPHeaderField: "Accept")
      return request
   }
}
