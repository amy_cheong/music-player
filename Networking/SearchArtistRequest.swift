//
//  SearchRequest.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 7/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import Foundation

class SearchArtistRequest: APIRequest {
   var method = RequestType.GET
   var path = "search"
   var parameters = [String: String]()
   
   init(term: String, offset: Int) {
      parameters["term"] = term
      parameters["media"] = "music"
      parameters["attribute"] = "artistTerm"
      parameters["limit"] = "25"
      parameters["offset"] = "\(offset*25)"
   }
}
