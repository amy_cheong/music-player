//
//  QueryServices.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 7/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import Foundation
import RxSwift

enum APIError: Error {
   case requestError
   case parseError
   
   var localizedDescription: String {
      switch self {
      case .requestError:
         return "Oops, something went wrong."
      case .parseError:
         return "Parsing error"
      }
   }
}

enum Result<Value> {
   case success(Value)
   case failure(Error)
}

class QueryService {
   
   private let baseURL = URL(string: "https://itunes.apple.com/")!
   
   func search(apiRequest: APIRequest) -> Single<Result<[Song]>> {

      return URLSession.shared.rx.response(request: apiRequest.request(with: self.baseURL))
         .retry(1)
         .flatMap { (response, data) -> Single<Result<[Song]>> in
            let validStatusCodeRange = 200..<300
            guard validStatusCodeRange.contains(response.statusCode) else {
               return Single.just(.failure(APIError.requestError))
            }
            
            let decoder = JSONDecoder()
            do {
               let model = try decoder.decode(SongList.self, from: data)
               return Single.just(.success(model.results))
            } catch {
               return Single.just(.failure(APIError.parseError))
            }
      }.asSingle()
   }
}
