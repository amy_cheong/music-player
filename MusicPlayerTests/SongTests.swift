//
//  SongTests.swift
//  MusicPlayerTests
//
//  Created by Amy Cheong on 10/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import XCTest
@testable import MusicPlayer

class SongTests: XCTestCase {
   func testSongMapping(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      
      XCTAssertEqual(songA.trackName, "T")
      XCTAssertEqual(songA.artistName, "A")
      XCTAssertEqual(songA.collectionName, "C")
      XCTAssertEqual(songA.artworkUrl100, "A")
      XCTAssertEqual(songA.previewUrl, "P")
   }
   
   func testSongTrackNameInequality(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "N", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      
      XCTAssertNotEqual(songA, songB)
   }
   
   func testSongArtistNameInequality(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "T", artistName: "B",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      
      XCTAssertNotEqual(songA, songB)
   }
   
   func testSongCollectionNameInequality(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "T", artistName: "A",
                       collectionName: "B", artworkUrl100: "A", previewUrl: "P")
      
      XCTAssertNotEqual(songA, songB)
   }
   
   func testSongArtworkUrlInequality(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "B", previewUrl: "P")
      
      XCTAssertNotEqual(songA, songB)
   }
   
   func testSongPreviewUrlInequality(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "B")
      
      XCTAssertNotEqual(songA, songB)
   }
}
