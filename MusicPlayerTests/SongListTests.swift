//
//  MusicPlayerTests.swift
//  MusicPlayerTests
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import XCTest
@testable import MusicPlayer

class SongListTests: XCTestCase {
   func testSongListMapping(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songList = SongList(results: [songA, songB])
      
      XCTAssertEqual(songList.results.count, 2)
      XCTAssertEqual(songList.results.first, songA)
      XCTAssertEqual(songList.results.last, songB)
   }
   
   func testSongListResultInequality(){
      let songA = Song(trackName: "T", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songB = Song(trackName: "N", artistName: "A",
                       collectionName: "C", artworkUrl100: "A", previewUrl: "P")
      let songListA = SongList(results: [songA])
      let songListB = SongList(results: [songB])
      
      XCTAssertNotEqual(songListA, songListB)
   }
}
