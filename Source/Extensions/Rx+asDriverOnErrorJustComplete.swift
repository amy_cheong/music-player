//
//  Rx+asDriverOnErrorJustComplete.swift
//  SEED
//
//  Created by Ryne Cheow on 14/10/2017.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

/// It's just a helper method to "ignore errors" while transforming any Observable to Driver.
//  It's more meaningfull to call `.asDriverOnErrorJustComplete()` than to `.asDriver(onErrorDriveWith: .empty())`.
/// It's just a syntactic sugar, however it reduces the number of implications which reader needs to process.
extension ObservableConvertibleType {
   func asDriverOnErrorJustComplete() -> Driver<E> {
      return asDriver(onErrorDriveWith: .empty())
   }
}
