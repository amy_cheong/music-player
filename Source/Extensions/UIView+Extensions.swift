//
//  UIView+Extensions.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//
import UIKit

extension UIView {
   func blurBackground(){
      let blurEffect = UIBlurEffect(style: .dark)
      let blurEffectView = UIVisualEffectView(effect: blurEffect)
      blurEffectView.frame = self.bounds
      
      blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
      self.addSubview(blurEffectView)
   }
   
   func displayShadow() {
      layer.shadowOffset = CGSize(width: 0, height: -4)
      layer.shadowRadius = 4 / UIScreen.main.nativeScale
      layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
      layer.shadowOpacity = 1.0
      layer.shouldRasterize = true
      layer.masksToBounds = false
      layer.rasterizationScale = UIScreen.main.nativeScale
   }
}
