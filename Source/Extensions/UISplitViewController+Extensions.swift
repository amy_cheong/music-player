//
//  UISplitViewController+Extension.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 9/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//
import UIKit

extension UISplitViewController {
   var primaryViewController: UIViewController? {
      return self.viewControllers.first
   }
   
   var secondaryViewController: UIViewController? {
      return self.viewControllers.count > 1 ? self.viewControllers[1] : nil
   }
}
