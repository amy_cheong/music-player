//
//  Rx+Void.swift
//  SEED
//
//  Created by Ryne Cheow on 13/5/17.
//  Copyright © 2018 Tigerspike. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension ObservableType {
   /// Discard value returning void instead
   func void() -> Observable<Void> {
      return self.map({ _ in () })
   }
}

extension SharedSequenceConvertibleType {
   /// Discard value returning void instead
   func void() -> SharedSequence<SharingStrategy, Void> {
      return self.map({ _ in () })
   }
}
