//
//  Rx+Not.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

extension ObservableType where E == Bool {
   /// Boolean not operator
   func not() -> Observable<Bool> {
      return map(!)
   }
}

extension SharedSequenceConvertibleType where E == Bool {
   /// Boolean not operator.
   func not() -> SharedSequence<SharingStrategy, Bool> {
      return map(!)
   }
}
