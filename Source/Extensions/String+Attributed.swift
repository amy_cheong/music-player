//
//  String+Attributed.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//


import UIKit

extension String {
   func attributedString(withStyle style: FontAttributedStyle,
                         paragraphSpacingBefore: CGFloat = 0,
                         textAlignment: NSTextAlignment = .natural,
                         underlineStyle: NSUnderlineStyle = .styleNone,
                         textColor: UIColor) -> NSAttributedString {
      return .init(string: self, attributes: style.attributes(withTextColor: textColor,
                                                              paragraphSpacingBefore: paragraphSpacingBefore,
                                                              underlineStyle: underlineStyle,
                                                              textAlignment: textAlignment))
   }
}
