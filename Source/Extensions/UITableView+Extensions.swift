//
//  UITableView+Extensions.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//


import UIKit

extension UITableView {
   func hideEmptyCells() {
      tableFooterView = UIView(frame: .zero)
   }
   
   /// Dequeue cell of a specified type.
   ///
   /// - Parameters:
   ///   - type: Type to return.
   ///   - indexPath: IndexPath of the cell.
   ///   - identifier: Optional identifier. If nil a description of the Type will be used.
   /// - Returns: Cell of specified type.
   func dequeueCell<T>(of type: T.Type, at indexPath: IndexPath, with identifier: String? = nil) -> T {
      let identifier = identifier ?? String(describing: type.self)
      
      guard let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T else {
         fatalError("\(identifier) is nil")
      }
      
      return cell
   }
   
   /// Register cell for dequeuing.
   ///
   /// - Parameters:
   ///   - type: Type to register.
   ///   - identifier: Optional identifier. If nil a description of the Type will be used.
   func registerCell<T>(of type: T.Type, with identifier: String? = nil) {
      register(type.self as? AnyClass, forCellReuseIdentifier: identifier ?? String(describing: type.self))
   }
   
   func deselectActiveRow() {
      if let indexPath = self.indexPathForSelectedRow {
         deselectRow(at: indexPath, animated: true)
      }
   }
}

extension UITableView {
   
   func isNearTheBottomEdge(startLoadingOffset: CGFloat,
                            contentOffset: CGPoint) -> Bool {      
      return contentOffset.y + self.frame.size.height + startLoadingOffset > self.contentSize.height
   }
}
