//
//  GenericEmptyType.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import UIKit

enum GenericEmptyType {
   case emptyResults
   case start
   
   var description: String {
      switch self {
      case .emptyResults:
         return "Oops, no songs were found"
      case .start:
         return "Let's start finding song to listen!"
      }
   }
   
   var image: UIImage {
      switch self {
      case .emptyResults:
         return ImageAsset.resultNotFoundIcon.image
      case .start:
         return ImageAsset.startSearchIcon.image
      }
   }
}
