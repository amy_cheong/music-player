//
//  MediaPlayerView.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation

extension AVPlayerTimeControlStatus {
   
   var buttonImage: UIImage {
      switch self {
      case .playing:
         return ImageAsset.pauseButton.image
      case .paused, .waitingToPlayAtSpecifiedRate:
         return ImageAsset.playButton.image
      }
   }
}

final class MediaPlayerView: UIView {
   private var disposeBag = DisposeBag()
   lazy var song = BehaviorSubject<Song?>(value: nil)
   lazy var status = BehaviorSubject<AVPlayerTimeControlStatus>(value: .paused)
   var buttonClicked: Driver<Void>!
   
   lazy var backgroundView: UIView = {
      let view = UIView()
      let blurEffect = UIBlurEffect(style: .dark)
      let blurEffectView = UIVisualEffectView(effect: blurEffect)
      blurEffectView.frame = view.frame
      return blurEffectView
   }()
   
   // Play or Pause
   private lazy var statusImageView: UIImageView = {
      let imageView = UIImageView()
      imageView.contentMode = .scaleAspectFit
      imageView.image = AVPlayerTimeControlStatus.paused.buttonImage
      return imageView
   }()
   
   lazy var selectButton: UIButton = {
      let view = UIButton()
      view.backgroundColor = .clear
      return view
   }()
   
   private lazy var titleLabel: UILabel = {
      let view = UILabel()
      view.lineBreakMode = .byTruncatingTail
      return view
   }()
   
   private lazy var artistLabel: UILabel = {
      let view = UILabel()
      view.lineBreakMode = .byTruncatingTail
      return view
   }()
   
   lazy var progressBar: UIProgressView = {
      let view = UIProgressView()
      view.tintColor = .white
      view.trackTintColor = .tundora
      return view
   }()
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      preservesSuperviewLayoutMargins = true
      backgroundColor = .clear
      layout()
      bind()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   func layout() {
      [backgroundView, statusImageView,
       selectButton, titleLabel, artistLabel, progressBar].forEach(addSubview)
      
      backgroundView.snp.makeConstraints { make in
         make.edges.equalToSuperview()
      }
      
      progressBar.snp.makeConstraints { make in
         make.bottom.equalToSuperview().inset(5.0)
         make.leading.trailing.equalToSuperview()
         make.height.equalTo(6.0)
      }
      
      statusImageView.snp.makeConstraints { make in
         make.centerY.equalToSuperview()
         make.leading.equalToSuperview().offset(20.0)
         make.size.equalTo(40)
      }
      
      selectButton.snp.makeConstraints { make in
         make.centerX.centerY.equalTo(statusImageView)
         make.size.equalTo(statusImageView)
      }
      
      titleLabel.snp.makeConstraints { make in
         make.top.equalToSuperview().offset(15.0)
         make.leading.equalTo(statusImageView.snp.trailing).offset(10.0)
         make.trailing.equalToSuperview().inset(20.0)
      }
      
      artistLabel.snp.makeConstraints { make in
         make.top.equalTo(titleLabel.snp.bottom).offset(3.0)
         make.leading.equalTo(statusImageView.snp.trailing).offset(10.0)
         make.trailing.equalToSuperview().inset(20.0)
      }
   }
   
   func bind(){
      buttonClicked = selectButton.rx.tap.asDriver()
      
      // update button if status changed
      status.asDriverOnErrorJustComplete()
         .drive(onNext: { [weak self] status in
            guard let `self` = self else { return }
            self.statusImageView.image = status.buttonImage
         }).disposed(by: disposeBag)
      
      song.filterNil().asDriverOnErrorJustComplete()
         .drive(onNext: { [weak self] song in
            
            self?.titleLabel.attributedText = song.trackName?
               .attributedString(withStyle: .ITC_Demi_Size13_LineHeight20, textColor: .whiteLilac)
            
            self?.artistLabel.attributedText = song.artistName?
               .attributedString(withStyle: .ITC_Book_Size13_LineHeight20, textColor: .whiteLilac)
            
         }).disposed(by: disposeBag)
   }
}

