//
//  PlayerListEmptyView.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//
import UIKit

final class GenericEmptyView: UIView {
   
   private lazy var imageView: UIImageView = {
      let imageView = UIImageView()
      imageView.contentMode = .center
      return imageView
   }()
   
   private lazy var descriptionLabel: UILabel = {
      let label = UILabel()
      return label
   }()
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      layout()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   override func layoutSubviews() {
      super.layoutSubviews()
      
      imageView.snp.makeConstraints { make in
         make.top.equalToSuperview()
         make.centerX.equalToSuperview()
         make.width.equalTo(175)
         make.height.equalTo(240)
      }
      
      descriptionLabel.snp.makeConstraints { make in
         make.top.equalTo(imageView.snp.bottom).offset(11.0)
         make.leading.trailing.equalToSuperview()
         make.centerX.equalTo(imageView.snp.centerX)
         make.bottom.equalToSuperview()
      }
   }
   
   func layout() {
      [imageView, descriptionLabel].forEach(addSubview)
   }
   
   func bind(type: GenericEmptyType) {
      imageView.image = type.image
      
      descriptionLabel.attributedText = type.description.attributedString(withStyle: .ITC_Demi_Size13_LineHeight20,
                                     textColor: UIColor.black.withAlphaComponent(0.4))
   }
}
