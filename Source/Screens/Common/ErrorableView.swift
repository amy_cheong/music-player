//
//  ErrorableView.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 12/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import Foundation
import UIKit

protocol Errorable  {
   func showAlert(with clientAPIError: APIError)
}

extension Errorable where Self: UIViewController {
   func showAlert(with error: APIError) {
      let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
      let alertViewController = UIAlertController(title: "Something went wrong",
                                                  message: error.localizedDescription, preferredStyle: .alert)
      alertViewController.addAction(okAlertAction)
      self.present(alertViewController, animated: true, completion: nil)
   }
}
