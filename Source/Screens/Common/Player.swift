//
//  Player.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 10/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import AVFoundation
import RxSwift
import RxCocoa

class Player: AVPlayer {
   private let disposeBag = DisposeBag()
   lazy var song = BehaviorSubject<Song?>(value: nil)
   var statusChanged = BehaviorSubject<Void?>(value: nil)
   var progressBarProgress: Driver<Float>!
   
   override init() {
      super.init()
      bind()
   }
   
   func bind(){
      
      /**
       Requirement: When we tap a song for the first time,
       a media player should show up at the bottom of the screen
       and optionally you can start to play the preview for that song.
       */
      song.filterNil()
         .take(1)
         .asDriverOnErrorJustComplete()
         .drive(onNext: { song in
            guard let previewUrlString = song.previewUrl,
               let previewUrl = URL(string: previewUrlString) else { return }
            let avPlayerItem = AVPlayerItem(url: previewUrl)
            self.replaceCurrentItem(with: avPlayerItem)
            self.play()
         }).disposed(by: disposeBag)
      
      // Requirement: For each subsequent song selected,
      // do no​t play the song automatically.
      song.filterNil().skip(1)
         .asDriverOnErrorJustComplete()
         .distinctUntilChanged()
         .drive(onNext: { song in
            self.pause()
            guard let previewUrlString = song.previewUrl,
               let previewUrl = URL(string: previewUrlString) else { return }
            let avPlayerItem = AVPlayerItem(url: previewUrl)
            self.replaceCurrentItem(with: avPlayerItem)
         }).disposed(by: disposeBag)
      
      progressBarProgress = self.rx.currentItem.filterNil()
         .distinctUntilChanged().asDriverOnErrorJustComplete()
         .flatMap{ item in
            self.setupProgressObservation(item: item)
      }
      
      // Player stop/play when user clicked input button
      statusChanged
         .filterNil()
         .withLatestFrom(self.rx.timeControlStatus)
         .asDriverOnErrorJustComplete()
         .drive(onNext: { status in
            
            if status == .playing {
               self.pause()
            } else {
               self.play()
            }

         }).disposed(by: disposeBag)
   
      // Reset the time when song finished playing
      NotificationCenter.default.rx
         .notification(NSNotification.Name.AVPlayerItemDidPlayToEndTime)
         .do(onNext: { _ in
            self.seek(to: kCMTimeZero)
         }).asDriverOnErrorJustComplete()
         .drive().disposed(by: disposeBag)
      
   }
   
   private func setupProgressObservation(item: AVPlayerItem) -> Driver<Float> {
      let interval = CMTime(seconds: 0.05, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
      return self.rx.periodicTimeObserver(interval: interval).map { currentTime -> Float in
         return self.progress(currentTime: currentTime,
                              duration: item.duration)
         }.asDriverOnErrorJustComplete()
   }
   
   private func progress(currentTime: CMTime, duration: CMTime) -> Float {
      if !duration.isValid || !currentTime.isValid {
         return 0
      }
      
      let totalSeconds = duration.seconds
      let currentSeconds = currentTime.seconds
      
      if !totalSeconds.isFinite || !currentSeconds.isFinite {
         return 0
      }
      
      let p = Float(min(currentSeconds/totalSeconds, 1))
      return p
   }
}
