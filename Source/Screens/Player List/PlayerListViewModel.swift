//
//  PlayerListViewModel.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import RxCocoa
import RxSwift
import RxOptional

/**
 Transform searchbar's searchText into
 our tableView's data source (playerListSections)
 */
class PlayerListViewModel {
   
   struct Input {
      let searchText: Driver<String>
      let loadNextPage: Driver<Bool>
   }
   
   struct Output {
      let playerListSections: Driver<[PlayerListSectionData]>
      let errorListener: BehaviorSubject<Error?>
   }
   
   func transform(input: Input) -> Output {
      let queryService = QueryService()
      var offset = 0
      var resultArray: [Song] = []
      let errorListener = BehaviorSubject<Error?>(value: nil)

      let playerListSections =
         Driver.combineLatest(input.loadNextPage, input.searchText)
            .flatMap { loadNextPage, searchTerm -> Driver<Result<[Song]>>  in

            if searchTerm.isEmpty {
               offset = 0
               resultArray = []
               return Driver.just(Result<[Song]>.success([]))
            } else {
               
               if !loadNextPage {
                  return Driver.empty()
               } else {
                  offset += 1
                  let searchRequest = SearchArtistRequest(term: searchTerm, offset: offset)
                  return queryService.search(apiRequest: searchRequest)
                     .asDriverOnErrorJustComplete()
                  
               }
            }
         }.map { result -> [PlayerListSectionData] in
            
            switch result {
            case .success(let songs):
               resultArray += songs
               errorListener.onNext(nil)
            case .failure(let error):
               errorListener.onNext(error)
            }
            
            return [PlayerListSectionData(header: "",
                                          items: resultArray)]
      }
      
      
      return Output(playerListSections: playerListSections,
                    errorListener: errorListener)
   }
   
}
