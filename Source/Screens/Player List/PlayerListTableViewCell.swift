//
//  PlayerListTableViewCell.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//
import SnapKit
import UIKit
import KFSwiftImageLoader
import AVFoundation
import RxSwift
import RxCocoa

class PlayerListTableViewCell: UITableViewCell {
   private var disposeBag = DisposeBag()
   static let identifier = String(describing: PlayerListTableViewCell.self)
   
   private lazy var titleLabel: UILabel = {
      let view = UILabel()
      view.numberOfLines = 0
      return view
   }()
   
   private lazy var artistLabel: UILabel = {
      let view = UILabel()
      view.numberOfLines = 0
      return view
   }()
   
   private lazy var albumNameLabel: UILabel = {
      let view = UILabel()
      view.numberOfLines = 0
      return view
   }()
   
   private lazy var albumImageView: UIImageView = {
      let view = UIImageView()
      view.contentMode = .scaleAspectFill
      view.clipsToBounds = true
      return view
   }()
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      contentView.preservesSuperviewLayoutMargins = true
      layout()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
   }
   
   override func prepareForReuse() {
      disposeBag = DisposeBag()
      backgroundColor = .white
      super.prepareForReuse()
   }
   
   func layout(){
      preservesSuperviewLayoutMargins = true
      contentView.preservesSuperviewLayoutMargins = true
      selectionStyle = .none
      
      [titleLabel, artistLabel,
       albumNameLabel, albumImageView].forEach(contentView.addSubview)
      
      albumImageView.snp.makeConstraints { make in
         make.leading.equalToSuperview().offset(15.0)
         make.top.equalToSuperview().offset(20.0)
         make.bottom.equalToSuperview().inset(20.0).priority(.low)
         make.width.equalTo(100.0)
         make.height.equalTo(albumImageView.snp.width)
      }
      
      titleLabel.snp.makeConstraints { make in
         make.top.equalToSuperview().offset(10.0)
         make.leading.equalTo(albumImageView.snp.trailing).offset(10.0)
         make.trailing.equalToSuperview().inset(20.0)
      }
      
      artistLabel.snp.makeConstraints { make in
         make.top.equalTo(titleLabel.snp.bottom).offset(3.0)
         make.leading.equalTo(albumImageView.snp.trailing).offset(10.0)
         make.trailing.equalToSuperview().inset(20.0)
      }
      
      albumNameLabel.snp.makeConstraints { make in
         make.top.equalTo(artistLabel.snp.bottom).offset(3.0)
         make.leading.equalTo(albumImageView.snp.trailing).offset(10.0)
         make.trailing.equalToSuperview().inset(20.0)
         make.bottom.lessThanOrEqualToSuperview().inset(20.0)
      }
      
   }
   
   func bind(with song: Song, selectedSong: BehaviorSubject<Song?>) {
      
      titleLabel.attributedText = song.trackName?
         .attributedString(withStyle: .ITC_Bold_Size15_LineHeight25_LetterSpacing0,
                           textColor: .black)
      artistLabel.attributedText = song.artistName?.attributedString(withStyle: .ITC_Demi_Size13_LineHeight20,
                                                           textColor: .black)
      albumNameLabel.attributedText = song.collectionName?.attributedString(withStyle: .ITC_Book_Size13_LineHeight20,
                                                                 textColor: .black)
      
      if let albumImageURLString = song.artworkUrl100 {
         albumImageView.loadImage(urlString: albumImageURLString)
      }
      
      // Visual display for current selected song
      // Requirement: When a song is being played, you must provide
      // some indicate in the list item that the song is being played.
      selectedSong.filterNil()
         .asDriverOnErrorJustComplete()
         .drive(onNext: { playingSong in
            self.backgroundColor = playingSong == song ? .whiteLilac : .white
         }).disposed(by: disposeBag)
   }
   
}
