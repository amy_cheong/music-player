//
//  PlayerListSectionData.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import RxDataSources

struct PlayerListSectionData {
   var header: String
   var items: [Item]
}

extension PlayerListSectionData: SectionModelType {
   typealias Item = Song
   
   init(original: PlayerListSectionData, items: [Item]) {
      self = original
      self.items = items
   }
}
