//
//  PlayerListViewController.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//


import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import AVFoundation

final class PlayerListViewController: UIViewController, UITableViewDelegate, Errorable {
   struct Metric {
      static let startLoadingOffset: CGFloat = 20.0
      static let mediaPlayerViewHeight: CGFloat = 80.0
      static var mediaPlayerViewInTransitionY: CGFloat = {
         let maxHeight = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
         return maxHeight - 75
      }()
   }
   
   // MARK: Properties
   private let disposeBag = DisposeBag()
   private var viewModel = PlayerListViewModel()
   private var dataSource: RxTableViewSectionedReloadDataSource<PlayerListSectionData>?
   private let isSearchMode = BehaviorSubject<Bool>(value: false)
   
   private lazy var player: Player = {
      let player = Player()
      player.volume = 0.6
      return player
   }()
   
   private lazy var searchBar: UISearchBar = {
      let view =  UISearchBar()
      view.placeholder = "Search by artist"
      return view
   }()
   
   private lazy var emptyView: GenericEmptyView = {
      let view = GenericEmptyView(frame: .zero)
      view.bind(type: .start)
      view.backgroundColor = .clear
      view.isHidden = false
      return view
   }()
   
   private lazy var tableView: UITableView = {
      let tableView = UITableView()
      tableView.delegate = nil
      tableView.dataSource = nil
      tableView.backgroundColor = .clear
      tableView.preservesSuperviewLayoutMargins = true
      tableView.separatorStyle = .none
      tableView.keyboardDismissMode = .onDrag
      tableView.register(PlayerListTableViewCell.self,
                         forCellReuseIdentifier: PlayerListTableViewCell.identifier)
      tableView.contentInset.bottom = Metric.mediaPlayerViewHeight
      
      return tableView
   }()
   
   private lazy var mediaPlayerView: MediaPlayerView = {
      // Because we allow rotation for the app,
      // UIScreen only gives the first loaded height and width
      // and does not update accordingly hence have to use max and min.
      let maxLength = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
      let minLength = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
      let view = MediaPlayerView(frame: CGRect(x: 0,
                                               y: maxLength,
                                               width: minLength,
                                               height: Metric.mediaPlayerViewHeight) )
      view.displayShadow()
      return view
   }()
   
   // MARK: Methods
   override func viewDidLoad() {
      super.viewDidLoad()
      title = "Music Player"
      view.backgroundColor = .whiteLilac
      layout()
      bind()
   }
   
   /**
    Layout: where we set constraints to UI
    */
   func layout() {
      // Table view layout
      self.view.addSubview(tableView)
      tableView.snp.makeConstraints({ make in
         make.edges.equalToSuperview()
      })
      
      // Search bar layout
      navigationItem.titleView = searchBar
      
      // Empty view state layout
      let backgroundView = UIView(frame: tableView.frame)
      backgroundView.addSubview(emptyView)
      
      emptyView.snp.makeConstraints({ make in
         make.center.equalTo(backgroundView)
      })
      tableView.backgroundView = backgroundView
      
      // Media Player layout
      self.view.addSubview(mediaPlayerView)
   }
   
   /**
    Bind: where we bind logic to UI
    */
   func bind() {
      // Transform searchbar's searchText into our tableView's data source
      let searchBarSearched = searchBar.rx.text.orEmpty
         .debounce(0.2, scheduler: MainScheduler.instance)
         .distinctUntilChanged()
         .asDriverOnErrorJustComplete()
      
      let loadNextPage = tableView.rx.contentOffset
         .flatMap { offset-> Driver<Bool>  in
            return Driver.just(self.tableView
               .isNearTheBottomEdge(startLoadingOffset: Metric.startLoadingOffset,
                                    contentOffset: offset))
         }.debounce(0.1, scheduler: MainScheduler.instance)
         .asDriver(onErrorJustReturn: false)
            
      
      let output = viewModel
         .transform(input: .init(searchText: searchBarSearched,
                                 loadNextPage: loadNextPage))
      
      bindTableView(with: output)
      bindSearchBar(with: output)
      bindError(with: output)
      
      // Bind playerList's media player view to player
      bindPlayerToMediaPlayerView(view: mediaPlayerView)
      
      // Bind songDetail's media player view to player
      if let splitVC = self.splitViewController,
         let detailVC = splitVC.secondaryViewController as? DetailViewController {
         self.bindPlayerToMediaPlayerView(view: detailVC.mediaPlayerView)
      }
      
      // Slide up the playerList's media player only ONCE
      mediaPlayerView.song
         .filterNil().take(1)
         .asDriverOnErrorJustComplete()
         .drive(onNext: { [weak self] albumImageURLString in
            UIView.animate(withDuration: 0.8, animations: {
               self?.mediaPlayerView.frame.origin.y = Metric.mediaPlayerViewInTransitionY
            }, completion: nil)
         }).disposed(by: disposeBag)
      
      // Handle Orientation Changed
      //
      NotificationCenter.default.rx
         .notification(NSNotification.Name.UIDeviceOrientationDidChange)
         .withLatestFrom(player.song)
         .filterNil()
         .do(onNext: { [weak self] song in
            guard let `self` = self else { return }
            
            // Hide portrait's media player for landscape mode
            self.mediaPlayerView.isHidden = UIDevice.current.orientation.isLandscape
            
            if UIDevice.current.orientation.isPortrait {
               self.mediaPlayerView.frame.origin.y = Metric.mediaPlayerViewInTransitionY
            }
         }).asDriverOnErrorJustComplete()
         .drive().disposed(by: disposeBag)
   }
   
   /**
    Bind table view with its datasource
    and delegate
    */
   private func bindTableView(with output: PlayerListViewModel.Output){
      
      let dataSource =
         RxTableViewSectionedReloadDataSource<PlayerListSectionData>(
            configureCell: { (sectionData, tableView, indexPath, item) -> UITableViewCell in
               let cell = tableView.dequeueCell(of: PlayerListTableViewCell.self,
                                                at: indexPath)
               cell.bind(with: item, selectedSong: self.mediaPlayerView.song)
               return cell
         })
      
      self.dataSource = dataSource
      
      output.playerListSections
         .drive(tableView.rx.items(dataSource: dataSource))
         .disposed(by: disposeBag)
      
      tableView.rx.itemSelected
         .map { indexPath in
            return (indexPath, dataSource[indexPath])
         }.subscribe(onNext: { [weak self] indexPath, item in
            guard let `self` = self else { return }
            self.player.song.onNext(item)
         }).disposed(by: disposeBag)
      
      tableView.rx
         .setDelegate(self)
         .disposed(by: disposeBag)
   }
   
   /**
    Show different view if the results
    is empty or user hasn't start searching yet.
    */
   private func bindSearchBar(with output: PlayerListViewModel.Output){
      searchBar.rx.textDidBeginEditing.asDriver().map{ true }
         .drive(isSearchMode).disposed(by: disposeBag)
      
      Driver
         .combineLatest(isSearchMode.asDriver(onErrorJustReturn: false),
                        output.playerListSections.map { $0.first?.items.isEmpty }.filterNil())
         .drive(onNext: { [weak self] isSearchMode, emptyResults in
            let searchTextEmpty = self?.searchBar.text?.isEmpty ?? true
            self?.emptyView.isHidden = true
            
            if searchTextEmpty {
               // Show start search empty state view
               self?.emptyView.bind(type: .start)
               self?.emptyView.isHidden = false
            } else {
               // Show empty search result empty state view
               if isSearchMode && emptyResults {
                  self?.emptyView.bind(type: .emptyResults)
                  self?.emptyView.isHidden = false
               }
            }
         }).disposed(by: disposeBag)
   }
   
   /**
    Bind playerList and songDetail's media player views to
    our universal player
    */
   private func bindPlayerToMediaPlayerView(view: MediaPlayerView){
      self.player.song.bind(to: view.song).disposed(by: disposeBag)
      
      self.player.rx.timeControlStatus.bind(to: view.status)
         .disposed(by: disposeBag)
      
      view.buttonClicked.drive(self.player.statusChanged)
         .disposed(by: disposeBag)
      
      self.player.progressBarProgress
         .drive(view.progressBar.rx.progress)
         .disposed(by: disposeBag)
   }
   
   private func bindError(with output: PlayerListViewModel.Output){
      output.errorListener
         .filterNil()
         .do(onNext: { [weak self] error in
            guard let error = error as? APIError else { return }
            self?.showAlert(with: error)
            return
      }).asDriverOnErrorJustComplete()
         .drive().disposed(by: disposeBag)
   }
   
}
