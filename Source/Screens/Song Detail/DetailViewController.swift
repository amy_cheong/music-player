//
//  DetailViewController.swift
//  POC
//
//  Created by Amy Cheong on 7/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import UIKit
import KFSwiftImageLoader
import AVFoundation
import RxSwift
import RxCocoa

class DetailViewController: UIViewController {
   private var disposeBag = DisposeBag()

   struct Metric {
      static let mediaPlayerViewHeight: CGFloat = 80.0
      static var mediaPlayerViewInTransitionY: CGFloat = {
         let maxHeight = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
         return maxHeight - 75
      }()
   }

   private lazy var albumImageView: UIImageView = {
      let view = UIImageView()
      view.contentMode = .scaleAspectFill
      return view
   }()
   
   lazy var mediaPlayerView: MediaPlayerView = {
      let view = MediaPlayerView(frame: .zero)
      view.isHidden = true
      view.displayShadow()
      return view
   }()
   
   override func viewDidLoad() {
      super.viewDidLoad()
      layout()
      bind()
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      albumImageView.clipsToBounds = true
      albumImageView.layer.masksToBounds = true
   }
   
   func layout(){
      view.backgroundColor = .stormGray
      [albumImageView, mediaPlayerView].forEach(view.addSubview)
      
      let minLength = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
      albumImageView.snp.makeConstraints { make in
         make.centerX.equalToSuperview()
         make.centerY.equalToSuperview()
         make.size.equalTo(minLength)
      }
      
      mediaPlayerView.snp.makeConstraints { make in
         make.height.equalTo(100)
         make.width.equalTo(minLength)
         make.bottomMargin.equalToSuperview()
         make.centerX.equalToSuperview()
      }

   }
   
   func bind(){
      mediaPlayerView.song.map{ $0?.artworkUrl100 }.filterNil().asDriverOnErrorJustComplete()
         .drive(onNext: { [weak self] albumImageURLString in
            guard let `self` = self else { return }
            
            self.mediaPlayerView.isHidden = false
            // To get a higher resolution image
            let biggerPicture = albumImageURLString
               .replacingOccurrences(of: "100x100bb", with: "512x512bb")
            
            self.albumImageView.loadImage(urlString: biggerPicture)
            
            // Set background change according to album image
            if let image = self.albumImageView.image {
               self.view.backgroundColor = UIColor(patternImage: image).withAlphaComponent(0.5)
            }
         }).disposed(by: disposeBag)
   }
}

