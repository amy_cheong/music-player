//
//  FontAttributedStyle.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//


import UIKit

struct FontFamily {
   enum ITCAvantGardeGothicStd: String {
      case demi = "ITCAvantGardeStd-Demi"
      case book = "ITCAvantGardeStd-Bk"
      case bold = "ITCAvantGardeStd-Bold"
      
      func font(size: CGFloat) -> UIFont {
         return UIFont(name: self.rawValue, size: size)!
      }
   }
}


enum FontAttributedStyle {
   case ITC_Demi_Size13_LineHeight20
   case ITC_Book_Size13_LineHeight20
   case ITC_Bold_Size15_LineHeight25_LetterSpacing0
   
   func attributes(withTextColor textColor: UIColor,
                   paragraphSpacingBefore: CGFloat = 0,
                   lineBreakMode: NSLineBreakMode = .byWordWrapping,
                   underlineStyle: NSUnderlineStyle = .styleNone,
                   textAlignment: NSTextAlignment = .natural) -> [NSAttributedStringKey: Any] {
      var attributes: [NSAttributedStringKey: Any] = [:]
      let paragraphStyle = NSMutableParagraphStyle()
      
      attributes[.foregroundColor] = textColor
      attributes[.underlineStyle] = underlineStyle.rawValue
      
      switch self {
      case .ITC_Demi_Size13_LineHeight20:
         attributes[.font] = FontFamily.ITCAvantGardeGothicStd.demi.font(size: 13.0)
         paragraphStyle.maximumLineHeight = 20.0
         paragraphStyle.minimumLineHeight = 20.0
         
      case .ITC_Book_Size13_LineHeight20:
         attributes[.font] = FontFamily.ITCAvantGardeGothicStd.book.font(size: 13.0)
         paragraphStyle.maximumLineHeight = 20.0
         paragraphStyle.minimumLineHeight = 20.0
         
      case .ITC_Bold_Size15_LineHeight25_LetterSpacing0:
         attributes[.font] = FontFamily.ITCAvantGardeGothicStd.bold.font(size: 15.0)
         attributes[.kern] = 0
         paragraphStyle.maximumLineHeight = 25.0
         paragraphStyle.minimumLineHeight = 25.0
      }
      
      paragraphStyle.alignment = textAlignment
      paragraphStyle.paragraphSpacingBefore = paragraphSpacingBefore
      paragraphStyle.lineBreakMode = lineBreakMode
      
      attributes.updateValue(paragraphStyle, forKey: .paragraphStyle)
      
      return attributes
   }
}

extension Dictionary where Key == NSAttributedStringKey {
   func toTypingAttributes() -> [String: Any] {
      var convertedDictionary = [String: Any]()
      
      for (key, value) in self {
         convertedDictionary[key.rawValue] = value
      }
      
      return convertedDictionary
   }
}

