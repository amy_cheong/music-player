//
//  Colors.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 8/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import UIKit

extension UIColor {
   
   static var whiteLilac: UIColor {
      return #colorLiteral(red: 0.8941176471, green: 0.8745098039, blue: 0.9254901961, alpha: 1)
   }
   
   static var stormGray: UIColor {
      return #colorLiteral(red: 0.4078431373, green: 0.4196078431, blue: 0.537254902, alpha: 1)
   }
   
   
   static var tundora: UIColor {
      return #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
   }
}




