//
//  ImageAssets.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 9/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import UIKit
import Foundation

enum ImageAsset {
   
   case resultNotFoundIcon
   case startSearchIcon
   case playButton
   case pauseButton
   
   var image: UIImage {
      switch self {
      case .resultNotFoundIcon: return #imageLiteral(resourceName: "ResultNotFoundImage")
      case .startSearchIcon: return #imageLiteral(resourceName: "StartSearchImage")
      case .playButton: return #imageLiteral(resourceName: "Play")
      case .pauseButton: return #imageLiteral(resourceName: "Pause")
      }
   }
}
