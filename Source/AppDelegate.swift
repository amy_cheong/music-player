//
//  AppDelegate.swift
//  MusicPlayer
//
//  Created by Amy Cheong on 5/9/18.
//  Copyright © 2018 Amy Cheong. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?

   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
            
      let window = UIWindow(frame: UIScreen.main.bounds)

      let splitViewController =  UISplitViewController()
      let mainNavigationController = UINavigationController(rootViewController: PlayerListViewController())
      
      // Requirement:
      // The layout should be in 2 halves in landscape
      splitViewController.viewControllers = [mainNavigationController, DetailViewController()]
      splitViewController.preferredDisplayMode = .allVisible
      splitViewController.delegate = self
      splitViewController.preferredPrimaryColumnWidthFraction = 0.5

      let navigationBar = UINavigationBar.appearance()
      navigationBar.prefersLargeTitles = true
      window.rootViewController = splitViewController
      window.makeKeyAndVisible()
      self.window = window
      
      return true
   }

   func applicationWillResignActive(_ application: UIApplication) {}

   func applicationDidEnterBackground(_ application: UIApplication) {}

   func applicationWillEnterForeground(_ application: UIApplication) {}

   func applicationDidBecomeActive(_ application: UIApplication) {}

   func applicationWillTerminate(_ application: UIApplication) {}


}


extension AppDelegate: UISplitViewControllerDelegate {
   func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
      return true
   }
}
