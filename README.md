![screenshot](demo/screenshots.png)

# Music Player

## Features
Demo video can be viewed inside project directory
`demo/DemoVideo.mp4`

This app uses the [iTunes Search API][itunes] to create a simple music player app that lets you search for an artist by name

[itunes]: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/ 

1. Search music by artist name
2. Play music by its preview track
3. Available in portrait and landscape mode (for iPhone plus)
4. Music is still playing when app is at background.
5. For subsequent song selected, song is stopped.
6. App loads 25 songs and load more songs when user scrolls to the bottom of the list.

### Project Setup
This app uses Cocoapods for external dependencies, all you need is to install the libraries with

```
   pod install
```
## Architecture
* [Model-View-ViewModel (MVVM)][mvvm]
* Motivated by "massive view controllers": MVVM considers `UIViewController` subclasses part of the View and keeps them slim by maintaining all state in the ViewModel.
* To learn more about it, check out Bob Spryn's [fantastic introduction][sprynthesis-mvvm].

[mvvm]: https://www.objc.io/issues/13-architecture/mvvm/
[sprynthesis-mvvm]: http://www.sprynthesis.com/2014/12/06/reactivecocoa-mvvm-introduction/

### Project Structure

All the files are being structured as follow: 
```
├── Networking         # This is the External layer    
├── Entity             # This is the Domain layer                  
├── Source             # This is the App layer                 
│   ├── Screens         
│   ├── Styling    
│   ├── Extensions   
│   └── Resources      
└── Unit Tests 
```

### Built with libraries
1. [RxSwift, RxCocoa, RxOptional][rxswift]: Reactive Programming with Swift 
3. [RxDatasSources][rxdatasources]: Table and Collection View Data Sources for RxSwift
4. [SnapKit][snapkit]: A DSL to make Auto Layout easy on both iOS and OS X.
5. [KFSwiftImageLoader][imageloader]: Swift async web image loader

[rxswift]: https://github.com/ReactiveX/RxSwift
[rxdatasources]:https://github.com/RxSwiftCommunity/RxDataSources
[snapkit]: https://github.com/SnapKit/SnapKit
[imageloader]: https://github.com/kiavashfaisali/KFSwiftImageLoader

### Code structures
#### View Controllers

```
func layout() { // Set auto layout contraints to the UI layout

} 

func bind() { // Bind app logic to UI layout

} 

```

#### View Model

```
class ViewModel {
   struct input(){ // Set auto layout contraints to the UI layout

   } 

   struct output(){ // Bind app logic to UI layout

   } 

   // Transform input(user interaction) to   
   // output(formatted data for UI)
   func transform(input: Input) -> Output{ 
   
   } 
}

```

### Unit tests
Domain layer is being tested, in this project only Entity folder is tested.

## Author
This project is created by [Amy Cheong][amycheong] at 2018

[amycheong]: http://amycheong19.com/

